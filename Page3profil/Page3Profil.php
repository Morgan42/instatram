<?php 

// Connexion à la base de données
    try {

        $bdd = new PDO('mysql:host='.(getenv('MYSQL_HOST') ?: 'localhost').';dbname=Instagram;charset=utf8', 'root', '');  
    }

    catch(Exception $e) {

            die('Erreur : '.$e->getMessage());
    }

//Récupération donnée BDD
    $reponse = $bdd->query('SELECT * FROM Profil');

?>

<!doctype html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
        crossorigin="anonymous">
    <link rel="stylesheet" href="PageProfil.css">

    <title> Profil</title>
</head>

<body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand bouttonNav" href="http://localhost/Projet_Instagram/Page_Accueil/Page_Accueil.php">Accueil</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>                  
                </button>   
                            
            <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item active">
                      <a class="nav-link" href="http://localhost/Projet_Instagram/Page_profil/PageProfil.php">Profil<span class="sr-only">(current)</span></a>
                    </li>                    
                  </ul>
                
                <form class="form-inline recherche">
                    <input class="form-control mr-sm-2" type="search" placeholder="non fonctionnel" aria-label="Search">
                    <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Recherche</button>
                </form>
                <span class="pseudo"> <?php echo $_COOKIE['Pseudo'] ?></span>
            </div>
        </nav>
    
    <div class="placeLogo">
        <img src='logo.gif'>
    </div>

    <div class="container">
        <div class="row">

        <?php foreach($reponse as $Profil) { ?>

            <div class="col-4">
            
                <div class="card hovercard bord">
                    <div class="cardheader">
                    </div>
                    <a href="/Projet_Instagram/Page_Profil/pageprofil.php?id_profil=<?=$Profil["id"]?>">
                    <div id="effet" class="avatar">
                        <img src="<?=$Profil["avatar"]?>">
                    </div>
                    </a>
                    <div class="info">
                        <div class="title">
                            <p><?=$Profil["nom"]?></p>
                        </div>
                        <div class="desc"><?=$Profil["metier"]?></div>
                    </div>
                </div>
            </div>
        <?php } ?>

        </div>
    </div>

    <nav class="navbar fixed-bottom navbar-dark bg-dark">
        <a class="navbar-brand"></a>
    </nav>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>

</html>