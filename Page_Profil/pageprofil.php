<?php 

// Connexion à la base de données
    try {
        $bdd = new PDO('mysql:host='.(getenv('MYSQL_HOST') ?: 'localhost').';dbname=Instagram;charset=utf8', 'root', '');  
    }

    catch(Exception $e) {
            die('Erreur : '.$e->getMessage());
    }

//Récupération des photos dans la BDD par id
$photos = $bdd->query('SELECT * FROM Photo WHERE id_profil='.$_GET['id_profil'].' ORDER BY id_profil ASC LIMIT 0,6');

//Récupération donnée BDD pour les profils
$profilQuery = $bdd->query('SELECT * FROM Profil WHERE id='.$_GET['id_profil'].' ORDER BY id ASC');


//Récupétation du profil en BDD
$profil = $profilQuery->fetch();

?>

<!doctype html>
<html lang="fr">

<head>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"><!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="pageprofil.css">

    <!-- Include the above in your HEAD tag -->

    <title>Instatram</title>
</head>

<body>

    <div class="container">
        <div class="row">           
            <div class="col-lg-12 col-sm-6">
                <div class="card hovercard bord">
                    <div class="cardheader">
                    </div>
                    <div class="avatar">
                        <img alt="" src="<?=$profil["avatar"]?>">
                    </div>
            
                    <div class="info">
                        <div class="title">
                            <p><?=$profil["nom"]?></p>
                        </div>
                        <div class="desc"><?=$profil["metier"]?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- galerie -->
  <section class="pad">
        <div class="container col">
            <div class="row pic2 try">
                <?php foreach($photos as $Photo) {
                    $likeQuery = $bdd->query('SELECT count(*) FROM likes WHERE id_photo='.$Photo["id"]);
                    $likeCount = $likeQuery->fetchColumn();
                ?>

                <div class="col-4">
                    <a href="#modal" data-toggle="modal" class="photoModal" data-id-photo="<?=$Photo["id"]?>" data-like-count="<?=$likeCount?>">
                        <img src="<?=$Photo["photo"]?>" width="400" heigth="300" alt="<?=$Photo["titre"]?>" />
                    </a>
                </div>

                <?php } ?>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="contentModal">   
                           
                 </div>
                 <div class= "commentForm">
                        <?php include'affichage_commentaire.php' ?> 
                    <div class="container">
                        <div class="row">                        
                            <form method="POST" action="TraitementCom.php" class="form-inline" onsubmit="storeMessage(event, this)">
                            <input type="hidden" name="id_photo" value="" id="inputHiddenIdPhoto"> 
                            <button disabled="disabled" class= "btn btn-dark col-3"><span class="pseudo"> <?=$_COOKIE['Pseudo'] ?></span></button>
                                    <input name="commentaire" type="text" class="form-control col-8" Placeholder= "Entrez votre commentaire"/>
                                    <button id="btn" class= "btn btn-danger col-1" type="submit" >Go</button>
                            </form>
                        </div>
                    </div>
                 </div>

                <div class="modal-footer">
                <form action="query.php" method="post">
                    <input type="hidden" name="id_photo" value="" id="inputHiddenIdPhoto">                    
                    <button type="button" id="likebutton" class="btn btn-dark">
                    <span id="likeCount"></span>
                    <i class="fas fa-heart"></i></button>               
                </form>
                    <button type="button" class="btn btn-primary" id="btn-comment"><i class="fas fa-comments"></i></button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="insta.js"></script>
</body>

</html>