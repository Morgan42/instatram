//Appel de la photo dans la modal
$(".photoModal").click(function() {
    $("#contentModal").html('<img src="'+ $(this).find("img").attr("src") +'" width="100%" heigth="100%" alt="Image ' + $(this).attr("id")+ '" /> ');
    $("#modalTitle").html($(this).children("img").attr("alt"));

    $("#inputHiddenIdPhoto").val(
        $(this).data('id-photo')
    )  
    $("#likeCount").html(
        $(this).data('like-count') 
    )      
});

// requete pour les likes
$("#likebutton").click(function(){
    // Je définis ma requête ajax
    $.post({
        // Adresse à laquelle la requête est envoyée
        url:'query.php',       
        data: {  
            id_photo: $(this).prev().val()
        },
        // La fonction à apeller si la requête aboutie
        success: function (error) {
            if(error) {
                alert(error);
            }
            $("#likeCount").html(
                parseInt($("#likeCount").html()) + 1
            )
        }
    });
});

$("#btn-comment").click(function(){
    $(".commentForm").css("display","block");
})

function storeMessage(event, form) {
    event.preventDefault();

    $.post({
       url: $(form).attr('action'),
       data: $(form).serialize(),
       success: function(error) {          
        location.reload();
        $("#modal").modal("show");
        $('#message').html('value', '');
       } 
    });
}
